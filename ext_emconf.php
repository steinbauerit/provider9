<?php
$EM_CONF[$_EXTKEY] = array(
  'title' => 'Provider',
  'description' => 'Provider extension for TYPO3 8 sites from Steinbauer IT',
  'category' => 'Example Extensions',
  'author' => 'Martin Lipp',
  'author_email' => 'martin.lipp@steinbauer-it.com',
  'author_company' => 'steinbauer-it.com',
  'shy' => '',
  'priority' => '',
  'module' => '',
  'state' => 'stable',
  'internal' => '',
  'uploadfolder' => '0',
  'createDirs' => '',
  'modify_tables' => '',
  'clearCacheOnLoad' => 0,
  'lockType' => '',
  'version' => '0.0.0',
  'constraints' =>
  array(
    'depends' =>
    array(
      'typo3' => '8.7.0-8.99.99',
    ),
    'conflicts' =>
    array(
    ),
    'suggests' =>
    array(
    ),
  ),
);
