var iOS = false, p = navigator.platform;
if( p === 'iPad' || p === 'iPhone' || p === 'iPod' ){
    iOS = true;
}

function resize() {
    winWidth = $(window).width();
    winHeight = $(window).height();
}

$(document).ready(function(){

    $('#toggle').click(function(){
        $(this).toggleClass('open');
    });

    /*$(window).scroll(function() {
        var scrollPos = $(document).scrollTop();
        if (scrollPos >= 155) {
            
        } else {
            
        }
    });*/

    /*var position = $(window).scrollTop();
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if(scroll > position) {
            console.log('down');
        } else  {
            console.log('up');
        }
        position = scroll;
    });*/

    /*$('.-carousel').slick({
        dots: true,
        infinite: true,
        speed: 800,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 4000,
        fade: false,
    });*/

    resize();

    if (winWidth>992) {
        window.sr = ScrollReveal({reset: false});
        sr.reveal('.main-content .content', {duration: 500});
    }

});

if (iOS == true) {
} else {
    $(window).resize(function() {
        resize();
    });
}