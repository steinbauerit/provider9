<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig("@import 'EXT:" . $_EXTKEY . "/Configuration/TSConfig/Page.typoscript'");

$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['mask'] = [
	'json' => 'EXT:provider/Resources/Private/Mask/mask.json',
	'content' => 'EXT:provider/Resources/Private/Mask/Frontend/Templates/',
	'layouts' => 'EXT:provider/Resources/Private/Mask/Frontend/Layouts/',
	'partials' => 'EXT:provider/Resources/Private/Mask/Frontend/Partials/',
	'backend' => 'EXT:provider/Resources/Private/Mask/Backend/Templates/',
	'layouts_backend' => 'EXT:provider/Resources/Mask/Backend/Layouts/',
	'partials_backend' => 'EXT:provider/Resources/Mask/Backend/Partials/',
	'preview' => 'EXT:provider/Resources/Private/Mask/Backend/Previews/'
];

// disable deprecation log
// $GLOBALS['TYPO3_CONF_VARS']['LOG']['TYPO3']['CMS']['deprecations']['writerConfiguration'][\TYPO3\CMS\Core\Log\LogLevel::NOTICE] = [];